//
//  UIScrollView+Extension.swift
//  ScreenshotSayeckLib
//
//  Created by Alejandro Enrique Campos Iriarte on 09/09/21.
//

import Foundation
import UIKit

extension UIScrollView {
    
    func makeSnapshot(superview: UIView) -> UIImage? {
        // MARK: Extra
        removeFromSuperview()
        // MARK: End extra

        UIGraphicsBeginImageContextWithOptions(contentSize, false, 0)

        contentOffset = .zero
        frame = CGRect(x: 0, y: 0, width: contentSize.width, height: contentSize.height)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // MARK: Extra
        translatesAutoresizingMaskIntoConstraints = false
        superview.addSubview(self)
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.topAnchor, constant: 10),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 10),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -10)
        ])
        // MARK: End extra
        
        return image
    }

}

//extension CALayer {
//
//    func makeSnapshot() -> UIImage? {
//        let scale = UIScreen.main.scale
//        UIGraphicsBeginImageContextWithOptions(frame.size, false, scale)
//        defer { UIGraphicsEndImageContext() }
//        guard let context = UIGraphicsGetCurrentContext() else { return nil }
//        render(in: context)
//        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
//
//        return screenshot
//    }
//
//}
//
//extension UIView {
//
//    func makeSnapshot() -> UIImage? {
//        if #available(iOS 10.0, *) {
//            let renderer = UIGraphicsImageRenderer(size: frame.size)
//            return renderer.image { _ in drawHierarchy(in: bounds, afterScreenUpdates: true) }
//        } else {
//            return layer.makeSnapshot()
//        }
//    }
//
//}
