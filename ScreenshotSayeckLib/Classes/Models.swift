//
//  Models.swift
//  ScreenshotSayeckLib
//
//  Created by Alejandro Enrique Campos Iriarte on 09/09/21.
//

import Foundation

struct ScreenshotData: Codable {
    let loader: Bool
    var effect: String
}

public enum Effect: Codable {
    case overlay, blur
}
