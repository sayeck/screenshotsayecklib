//
//  ScreenshotSayeckLib.swift
//  ScreenshotSayeckLib
//
//  Created by Alejandro Enrique Campos Iriarte on 08/09/21.
//

import Foundation
import UIKit

@objc(ScreenshotSayeckLib)
public final class ScreenshotSayeckLib: NSObject {
    
    // MARK: - Public properties -
    public static let shared: ScreenshotSayeckLib = ScreenshotSayeckLib()
    public static var overlayAlpha: CGFloat = 0.5
    public static var gifSize: CGFloat = 100
    public static var gifName: String = "loader@2x"
    
    typealias ResultScreenshot = Result<Bool, ScreenshotError>
    
//    public static let shared: ScreenshotSayeckLib = ScreenshotSayeckLib()
//    public struct config {
//        static var overlayAlpha: CGFloat = 0.5
//        static var gifSize: CGFloat = 100
//        static var gifName: String = "loader@2x"
//    }
    
    // MARK: - Public methods -
    
    public func takeScreenshot(
        controller: UIViewController,
        view: UIScrollView,
        loader: Bool = true,
        effect: Effect = .overlay,
        completion: @escaping (UIImage?) -> Void
    ) {
        guard let superview = view.superview else { return }
        
        if loader {
            _showLoader(with: controller, effect: effect) { result in
                switch result {
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                case .success(_):
                    completion(view.makeSnapshot(superview: superview))
                }
            }
        } else {
            completion(view.makeSnapshot(superview: superview))
        }
    }
    
    public func share(controller: UIViewController, image: UIImage) {
        var imageToShare = [AnyObject]()
        imageToShare.append(image)
        
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = controller.view
        controller.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: - Private methods -
    
    private func _showLoader(
        with controller: UIViewController,
        effect: Effect = .overlay,
        completion: @escaping (ResultScreenshot) -> Void
    ) {
        let view = UIView()
        view.frame = controller.view.frame
        controller.view.addSubview(view)
        
        switch effect {
        case .overlay:
            view.backgroundColor = .white.withAlphaComponent(ScreenshotSayeckLib.overlayAlpha)
        case .blur:
            _setupBlurEffect(with: view, intensity: ScreenshotSayeckLib.overlayAlpha)
        }
        
        _setupLoaderGif(view: view) { result in
            switch result {
            case.failure(let error):
                completion(.failure(error))
            case .success(let success):
                completion(.success(success))
            }
        }
    }
    
    private func _setupLoaderGif(view: UIView, completion: @escaping (ResultScreenshot) -> Void) {
        let imageView = UIImageView(frame: CGRect(
            x: view.frame.midX - (ScreenshotSayeckLib.gifSize / 2),
            y: view.frame.midY - (ScreenshotSayeckLib.gifSize / 2),
            width: ScreenshotSayeckLib.gifSize,
            height: ScreenshotSayeckLib.gifSize
        ))
        
        do {
            let image = try UIImage(gifName: ScreenshotSayeckLib.gifName)
            imageView.setGifImage(image)
            view.addSubview(imageView)
    
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                view.removeFromSuperview()
                completion(.success(true))
            }
        } catch {
            view.removeFromSuperview()
            completion(.failure(ScreenshotError.notFound(ScreenshotSayeckLib.gifName)))
        }
    }
    
    private func _setupBlurEffect(with view: UIView, intensity: CGFloat) {
        let blurEffect = UIBlurEffect(style: .light)
//        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        let blurredEffectView = CustomVisualEffectView(effect: blurEffect, intensity: 0.1)
        blurredEffectView.frame = view.bounds
        view.addSubview(blurredEffectView)
    }
    
}

// MARK: Extensions

@objc(ScreenshotSayeckLib)
public extension ScreenshotSayeckLib {
    
    // Call from webbridge
    class func hybridInterface(_ json: String) {
        print(json)
    }
    
    // Call from UiViewConstroller
    func hybridInterface(_ json: String, controller: UIViewController, view: UIScrollView) {
        do {
            guard let jsonData = json.data(using: .utf8) else { return }
            let data = try JSONDecoder().decode(ScreenshotData.self, from: jsonData)

            var effect: Effect
            
            switch data.effect {
            case "overlay":
                effect = .overlay
            case "blur":
                effect = .blur
            default:
                effect = .overlay
            }
            
            takeScreenshot(controller: controller, view: view, loader: data.loader, effect: effect) { image in
                guard let image = image else { return }

                self.share(controller: controller, image: image)
            }
        } catch {
            print("Error: \(ScreenshotError.wrongJsonFormat.localizedDescription)")
        }
    }
    
}
