//
//  Errors.swift
//  ScreenshotSayeckLib
//
//  Created by Alejandro Enrique Campos Iriarte on 08/09/21.
//

import Foundation

enum ScreenshotError: Error {
    case unexpected
    case wrongJsonFormat
    case notFound(_ file: String?)
}

extension ScreenshotError: LocalizedError {
    
    var localizedDescription: String {
        switch self {
        case .unexpected:
            return NSLocalizedString("An unexpected error occurred", comment: "")
        case .wrongJsonFormat:
            return NSLocalizedString("Wrong json format.", comment: "")
        case .notFound(let file):
            return NSLocalizedString("File not found in blundle: \(file ?? "")", comment: "")
        }
    }
    
}
