//
//  ViewController.swift
//  ScreenshotSayeckLibExamples
//
//  Created by Alejandro Enrique Campos Iriarte on 08/09/21.
//

import UIKit
import ScreenshotSayeckLib

class ViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func screenshotButtonHandler(_ sender: Any) {
        ScreenshotSayeckLib.gifName = "anotherLoader@2x"
//        ScreenshotSayeckLib.shared.takeScreenshot(controller: self, view: scrollView, loader: true) { image in
//            guard let image = image else { return }
//
//            ScreenshotSayeckLib.shared.share(controller: self, image: image)
//        }
        
        let json = """
        {
        "loader":true,
        "effect":"blur"
        }
        """
        ScreenshotSayeckLib.shared.hybridInterface(json, controller: self, view: scrollView)
    }

}
